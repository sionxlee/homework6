﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Homework6
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            var initialMapLocation = MapSpan.FromCenterAndRadius(new Position(33.129385, -117.158815), Distance.FromMiles(1));

            MySampleMap.MoveToRegion(initialMapLocation);

            PopulatePicker();
        }
        private ObservableCollection<Pin> pins;
        private void PopulatePicker()
        {
            pins = new ObservableCollection<Pin>()
            {
                new Pin{
                Type = PinType.Place,
                Position = new Position(33.191737, -117.388697),
                Label = "Ruby's Dinner",
                Address = "On Ocean"
            },new Pin{
                Type = PinType.Place,
                Position = new Position(33.181889, -117.293053),
                Label = "Teri Cafe",
                Address = "Very Good Japanese restruant"
            },new Pin{
                Type = PinType.Place,
                Position = new Position(33.094437, -117.002497),
                Label = "Safari Zoo Park",
                Address = "Good place to be"
            },new Pin{
                Type = PinType.Place,
                Position = new Position(32.714761, -117.174676),
                Label = "USS Midway Museum",
                Address = "Historical Place"
            },new Pin{
               
                Type = PinType.Place,
                Position = new Position(32.735138, -117.148314),
                Label = "San Diego Zoo",
                Address = "Great Panda is there"
            },
            };

            //PinPicker.ItemsSource = pins;
            foreach (Pin item in pins)
            {
                PinPicker.Items.Add(item.Label);
                MySampleMap.Pins.Add(item);
            }

            PinPicker.SelectedIndex = 0;
            
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            
            Pin p=pins[PinPicker.SelectedIndex];
            MySampleMap.MoveToRegion(MapSpan.FromCenterAndRadius(p.Position, Distance.FromMiles(1)));
        }
        void SatelliteViewClicked(object sender, System.EventArgs e)
        {
            MySampleMap.MapType = MapType.Satellite;
        }
        void HybridViewClicked(object sender, System.EventArgs e)
        {
            MySampleMap.MapType = MapType.Hybrid;
        }
        void StreetViewClicked(object sender, System.EventArgs e)
        {
            MySampleMap.MapType = MapType.Street;
        }
    }
}
